package main

import (
	"fmt"
	"strconv"
)

func main() {
	// soal 1
	fmt.Println("=======SOAL 1=======")

	var panjangPersegiPanjang string = "8"
	var lebarPersegiPanjang string = "5"

	var alasSegitiga string = "6"
	var tinggiSegitiga string = "7"

	var panjang, _ = strconv.Atoi(panjangPersegiPanjang)
	var lebar, _ = strconv.Atoi(lebarPersegiPanjang)

	alas, _ := strconv.Atoi(alasSegitiga)
	tinggi, _ := strconv.Atoi(tinggiSegitiga)

	var kelilingPersegiPanjang int
	var luasSegitiga int

	kelilingPersegiPanjang = 2 * (panjang + lebar)
	luasSegitiga = (alas * tinggi) / 2
	// luasSegitiga = int(0.5 * (float32(alas) * float32(tinggi)))

	fmt.Printf("Keliling Persegi Panjang dari panjang %v dan lebar %v adalah %v", panjang, lebar, kelilingPersegiPanjang)
	fmt.Println()
	fmt.Printf("luas segitiga dari alas %v dan tinggi %v adalah %v", alas, tinggi, luasSegitiga)
	fmt.Println()

	// soal 2
	fmt.Println("=======SOAL 2=======")

	var nilaiJohn = 80
	var nilaiDoe = 50

	if nilaiJohn >= 80 {
		fmt.Println("Nilai John adalah A")
	} else if nilaiJohn >= 70 && nilaiJohn < 80 {
		fmt.Println("Nilai John adalah B")
	} else if nilaiJohn >= 60 && nilaiJohn < 70 {
		fmt.Println("Nilai John adalah C")
	} else if nilaiJohn >= 50 && nilaiJohn < 60 {
		fmt.Println("Nilai John adalah D")
	} else {
		fmt.Println("Nilai John adalah E")
	}

	if nilaiDoe >= 80 {
		fmt.Println("Nilai Doe adalah A")
	} else if nilaiDoe >= 70 && nilaiDoe < 80 {
		fmt.Println("Nilai Doe adalah B")
	} else if nilaiDoe >= 60 && nilaiDoe < 70 {
		fmt.Println("Nilai Doe adalah C")
	} else if nilaiDoe >= 50 && nilaiDoe < 60 {
		fmt.Println("Nilai Doe adalah D")
	} else {
		fmt.Println("Nilai Doe adalah E")
	}

	fmt.Println()

	// soal 3
	fmt.Println("=======SOAL 3=======")

	var tanggal = 17
	var bulan = 8
	var tahun = 1945

	var strBulan string

	switch bulan {
	case 1:
		{
			strBulan = "Januari"
		}
	case 2:
		{
			strBulan = "Februari"
		}
	case 3:
		{
			strBulan = "Maret"
		}
	case 4:
		{
			strBulan = "April"
		}
	case 5:
		{
			strBulan = "Mei"
		}
	case 6:
		{
			strBulan = "Juni"
		}
	case 7:
		{
			strBulan = "Juli"
		}
	case 8:
		{
			strBulan = "Agustus"
		}
	case 9:
		{
			strBulan = "September"
		}
	case 10:
		{
			strBulan = "Oktober"
		}
	case 11:
		{
			strBulan = "November"
		}
	case 12:
		{
			strBulan = "Desember"
		}
	}

	fmt.Println(tanggal, strBulan, tahun)
	fmt.Println()

	// soal 4
	fmt.Println("=======SOAL 4=======")

	// if tahun >= 1944 && tahun <= 1964 {
	// 	fmt.Println("Baby Boomer")
	// } else if tahun >= 1965 && tahun <= 1979 {
	// 	fmt.Println("Generasi X")
	// } else if tahun >= 1980 && tahun <= 1994 {
	// 	fmt.Println("Generasi Y")
	// } else {
	// 	fmt.Println("Generasi Z")
	// }

	switch {
	case tahun >= 1944 && tahun <= 1964:
		{
			fmt.Println("Baby Boomer")
		}
	case tahun >= 1965 && tahun <= 1979:
		{
			fmt.Println("Generasi X")
		}
	case tahun >= 1980 && tahun <= 1994:
		{
			fmt.Println("Generasi Y")
		}
	case tahun >= 1980 && tahun <= 1994:
		{

			fmt.Println("Generasi Z")
		}
	default:
		{
			fmt.Println("Inputan Salah min 1944 dan maksimal 2015")
		}

	}

	fmt.Println()

}
