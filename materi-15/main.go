package main

import (
	"api-mysql/functions"
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func main() {
	router := httprouter.New()
	router.GET("/movie", functions.GetMovie)
	router.POST("/movie/create", functions.PostMovie)
	router.PUT("/movie/:id/update", functions.UpdateMovie)
	router.DELETE("/movie/:id/delete", functions.DeleteMovie)

	router.GET("/age-rating-category", functions.GetRating)
	router.POST("/age-rating-category/create", functions.PostRating)
	router.PUT("/age-rating-category/:id/update", functions.UpdateRating)
	router.DELETE("/age-rating-category/:id/delete", functions.DeleteRating)

	fmt.Println("Server Running At Port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
