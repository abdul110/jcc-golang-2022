package query

import (
	"api-mysql/config"
	"api-mysql/models"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
)

const (
	movie_table = "movie"
)

//GetAllMovie
func GetAllMovie(ctx context.Context) ([]models.Movie, error) {
	var movies []models.Movie
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Cannot connect to mysql", err)
	}

	// query
	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", movie_table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var movie models.Movie
		var createdAt, updatedAt string
		if err = rowQuery.Scan(&movie.ID,
			&movie.Title,
			&movie.Year,
			&createdAt,
			&updatedAt,
			&movie.AgeRatingCategoryId,
		); err != nil {
			return nil, err
		}

		movie.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		movie.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

		if err != nil {
			log.Fatal(err)
		}

		movies = append(movies, movie)
	}
	return movies, nil
}

//InsertMovie
func InsertMovie(ctx context.Context, movie models.Movie) error {
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Cannot connect to mysql", err)
	}

	// query
	queryText := fmt.Sprintf("INSERT INTO %v (title, year, created_at, updated_at, age_rating_category_id) values ('%v', %v, NOW(), NOW(), %v)", movie_table,
		movie.Title, movie.Year, movie.AgeRatingCategoryId)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}

	return nil
}

//UpdateMovie
func UpdateMovie(ctx context.Context, movie models.Movie, idMovie string) error {
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Cannot connect to mysql", err)
	}

	// query
	queryText := fmt.Sprintf("UPDATE %v SET title='%v', year=%v, updated_at= NOW(), age_rating_category_id=%v where id=%v", movie_table,
		movie.Title, movie.Year, movie.AgeRatingCategoryId, idMovie)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}

	return nil
}

//DeleteMovie
func DeleteMovie(ctx context.Context, idMovie string) error {
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Cannot connect to mysql", err)
	}

	// query
	queryText := fmt.Sprintf("DELETE FROM %v where id=%v", movie_table, idMovie)
	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()

	if check == 0 {
		return errors.New("id tidak ada")
	}

	if err != nil {
		fmt.Println(err.Error())
	}

	return nil
}
