package query

import (
	"api-mysql/config"
	"api-mysql/models"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
)

const (
	rating_table   = "age_rating_category"
	layoutDateTime = "2006-01-02 15:04:05"
)

//GetAllRating
func GetAllRating(ctx context.Context) ([]models.AgeRatingCategory, error) {
	var ratings []models.AgeRatingCategory
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Cannot connect to mysql", err)
	}

	// query
	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", rating_table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var rating models.AgeRatingCategory
		var createdAt, updatedAt string
		if err = rowQuery.Scan(&rating.ID,
			&rating.Name,
			&rating.Description,
			&createdAt,
			&updatedAt); err != nil {
			return nil, err
		}

		rating.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		rating.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

		if err != nil {
			log.Fatal(err)
		}

		ratings = append(ratings, rating)
	}
	return ratings, nil
}

//InsertRating
func InsertRating(ctx context.Context, rating models.AgeRatingCategory) error {
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Cannot connect to mysql", err)
	}

	// query
	queryText := fmt.Sprintf("INSERT INTO %v (name, description, created_at, updated_at) values ('%v', '%v', NOW(), NOW())", rating_table,
		rating.Name, rating.Description)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}

	return nil
}

//UpdateRating
func UpdateRating(ctx context.Context, rating models.AgeRatingCategory, idRating string) error {
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Cannot connect to mysql", err)
	}

	// query
	queryText := fmt.Sprintf("UPDATE %v SET name='%v', description='%v', updated_at= NOW() where id=%v", rating_table,
		rating.Name, rating.Description, idRating)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}

	return nil
}

//DeleteRating
func DeleteRating(ctx context.Context, idRating string) error {
	db, err := config.MySQL()

	// config db check
	if err != nil {
		log.Fatal("Cannot connect to mysql", err)
	}

	// query
	queryText := fmt.Sprintf("DELETE FROM %v where id=%v", rating_table, idRating)
	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()

	if check == 0 {
		return errors.New("id tidak ada")
	}

	if err != nil {
		fmt.Println(err.Error())
	}

	return nil
}
