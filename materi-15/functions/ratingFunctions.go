package functions

import (
	"api-mysql/models"
	"api-mysql/query"
	"api-mysql/utils"
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func GetRating(rw http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	ratings, err := query.GetAllRating(ctx)

	if err != nil {
		fmt.Println(err)
	}

	utils.ResponseJSON(rw, ratings, http.StatusOK)

}

//PostRating
func PostRating(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// pengecekan inputannya JSON atau bukan
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan Content-Type application/json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var rating models.AgeRatingCategory

	if err := json.NewDecoder(r.Body).Decode(&rating); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}

	if err := query.InsertRating(ctx, rating); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"Status": "Successfully",
	}

	utils.ResponseJSON(rw, res, http.StatusOK)

}

// UpdateRating
func UpdateRating(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// pengecekan inputannya JSON atau bukan
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan Content-Type application/json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var rating models.AgeRatingCategory

	if err := json.NewDecoder(r.Body).Decode(&rating); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}

	var idRating = ps.ByName("id")

	if err := query.UpdateRating(ctx, rating, idRating); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"Status": "Successfully",
	}

	utils.ResponseJSON(rw, res, http.StatusOK)

}

// DeleteRating
func DeleteRating(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var idRating = ps.ByName("id")

	if err := query.DeleteRating(ctx, idRating); err != nil {
		errDelete := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(rw, errDelete, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"Status": "Successfully",
	}

	utils.ResponseJSON(rw, res, http.StatusOK)

}
