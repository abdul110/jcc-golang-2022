package main

import "fmt"

func main()  {
	// soal 1
	fmt.Println("===== soal 1 =====")
	for i:=1; i <= 20; i++ {
		if (i%3 == 0 && i%2 == 1) {
			fmt.Print(i, " - ")
			fmt.Println("I Love Coding")
			} else if (i%2 == 0) {
				fmt.Print(i, " - ")
				fmt.Println("Candradimuka")
				} else {
			fmt.Print(i, " - ")
			fmt.Println("JCC")
		}
	}
	fmt.Println()
	
	// soal 2
	fmt.Println("===== soal 2 =====")
	for i:= 1; i <= 7; i++ {
		for j:=1; j<= i; j++ {
			fmt.Print("#")
		}
		fmt.Println()
	}
	fmt.Println()

	// soal 3
	fmt.Println("===== soal 3 =====")

	var kalimat = [...]string{"aku", "dan", "saya", "sangat", "senang", "belajar", "golang"}

	fmt.Println(kalimat[2:])
	fmt.Println()

	// soal 4
	fmt.Println("===== soal 4 =====")
	var sayuran = []string{}

	sayuran = append(sayuran, 
		"Bayam",
		"Buncis",
		"Kangkung",
		"Kubis",
		"Seledri",
		"Tauge",
		"Timun",
	)

	for index, sayur := range sayuran {
		fmt.Printf("%v. %v", index+1, sayur)
		fmt.Println()
	}
	fmt.Println()

	// soal 5
	fmt.Println("===== soal 5 =====")

	var satuan = map[string]int{
		"panjang": 7,
		"lebar":   4,
		"tinggi":  6,
	}

	volumBalok := 1
	for key, value := range satuan {
		fmt.Print(key, " = ")
		fmt.Println(value)
		volumBalok = volumBalok * value 
	}

	fmt.Println("volumBalok =", volumBalok)
}