package main

import (
	"api-mysql/functions"
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func main() {
	router := httprouter.New()
	router.GET("/movie", functions.GetMovie)
	router.POST("/movie", functions.PostMovie)
	router.PUT("/movie/:id", functions.UpdateMovie)
	router.DELETE("/movie/:id", functions.DeleteMovie)

	fmt.Println("Server Running At Port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
