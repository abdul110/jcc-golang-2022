package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// soal 1
	fmt.Println("=======SOAL 1=======")

	wordJabar := "Jabar"
	wordCoding := "Coding"
	wordCamp := "Camp"
	wordGolang := "Golang"
	word2022 := "2022"

	fmt.Println(wordJabar, wordCoding, wordCamp, wordGolang, word2022)
	fmt.Println()

	// soal 2
	fmt.Println("=======SOAL 2=======")

	halo := "Halo Dunia"

	halo = strings.Replace(halo, "Dunia", "Golang", 1)

	fmt.Println(halo)
	fmt.Println()

	// soal 3
	fmt.Println("=======SOAL 3=======")

	var kataPertama = "saya"
	var kataKedua = "senang"
	var kataKetiga = "belajar"
	var kataKeempat = "golang"

	kataKedua = strings.Title(kataKedua)
	kataKetiga = kataKetiga[:6] + strings.ToUpper(kataKetiga[len(kataKetiga)-1:])
	kataKeempat = strings.ToUpper(kataKeempat)

	fmt.Println(kataPertama, kataKedua, kataKetiga, kataKeempat)
	fmt.Println()

	// soal 4
	fmt.Println("=======SOAL 4=======")

	var angkaPertama = "8"
	var angkaKedua = "5"
	var angkaKetiga = "6"
	var angkaKeempat = "7"

	var firstNumber, _ = strconv.Atoi(angkaPertama)
	var secondNumber, _ = strconv.Atoi(angkaKedua)
	var thirdNumber, _ = strconv.Atoi(angkaKetiga)
	var fourthNumber, _ = strconv.Atoi(angkaKeempat)

	var sum = firstNumber + secondNumber + thirdNumber + fourthNumber

	fmt.Println(sum)
	fmt.Println()

	// soal 5
	fmt.Println("=======SOAL 5=======")

	kalimat := "halo halo bandung"
	angka := 2022

	kalimat = fmt.Sprintf("%q - %v", strings.ReplaceAll(kalimat, "halo", "hi"), angka)

	fmt.Println(kalimat)
	fmt.Println()

}
