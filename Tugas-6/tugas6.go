package main

import "fmt"

func main() {

	// soal 1
	fmt.Println("===== SOAL 1 =====")

	var luasLingkaran float64
	var kelilingLingkaran float64
	jariJari := 7.0

	hitungLingkaran(&luasLingkaran, &kelilingLingkaran, jariJari)

	fmt.Println("luas lingkaran dari jari jari", jariJari, "adalah", luasLingkaran)
	fmt.Println("keliling lingkaran dari jari jari", jariJari, "adalah", kelilingLingkaran)

	// soal 2
	fmt.Println()
	fmt.Println("===== SOAL 2 =====")

	var sentence string
	introduce(&sentence, "John", "laki-laki", "penulis", "30")

	fmt.Println(sentence) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"
	introduce(&sentence, "Sarah", "perempuan", "model", "28")

	fmt.Println(sentence) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun

	// soal 3
	fmt.Println()
	fmt.Println("===== SOAL 3 =====")

	var buah = []string{}

	tambahBuah(&buah, "Jeruk", "Semangka", "Mangga", "Strawberry", "Durian", "Manggis", "Alpukat")

	for index, item := range buah {
		fmt.Print(index + 1)
		fmt.Println(".", item)
	}

	// soal 4
	fmt.Println()
	fmt.Println("===== SOAL 4 =====")
	var dataFilm = []map[string]string{}

	tambahDataFilm("LOTR", "2 jam", "action", "1999", &dataFilm)
	tambahDataFilm("avenger", "2 jam", "action", "2019", &dataFilm)
	tambahDataFilm("spiderman", "2 jam", "action", "2004", &dataFilm)
	tambahDataFilm("juon", "2 jam", "horror", "2004", &dataFilm)

	for index, item := range dataFilm {
		var number = fmt.Sprintf("%v.", index+1)
		var i = 1
		for key, val := range item {
			if i == 1 {
				fmt.Println(number, key+":", val)
			} else {
				fmt.Println("   "+key+":", val)
			}
			i++
		}
		fmt.Println()
	}
}

// soal 1
func hitungLingkaran(luas *float64, keliling *float64, jariJari float64) {
	*luas = (22 * jariJari * jariJari) / 7
	*keliling = (22 * jariJari * 2) / 7
}

// soal 2
func introduce(sentence *string, name, gender, job, age string) {
	var prefix string

	if gender == "perempuan" {
		prefix = "Bu"
	} else {
		prefix = "Pak"
	}

	*sentence = fmt.Sprintf("%v %v adalah seorang %v yang berusia %v tahun", prefix, name, job, age)
}

// soal 3
func tambahBuah(buah *[]string, buahList ...string) {
	*buah = append(*buah, buahList...)
}

func tambahDataFilm(title, duration, genre, year string, dataFilm *[]map[string]string) {
	film := map[string]string{
		"title":    title,
		"duration": duration,
		"genre":    genre,
		"year":     year,
	}

	*dataFilm = append(*dataFilm, film)
}
