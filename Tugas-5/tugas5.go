package main

import "fmt"

func main() {
	// soal 1
	fmt.Println("=====SOAL 1=====")
	panjang := 12
	lebar := 4
	tinggi := 8

	luas := luasPersegiPanjang(panjang, lebar)
	keliling := kelilingPersegiPanjang(panjang, lebar)
	volume := volumeBalok(panjang, lebar, tinggi)

	fmt.Println(luas)
	fmt.Println(keliling)
	fmt.Println(volume)
	fmt.Println()

	// soal 2
	fmt.Println("=====SOAL 2=====")
	john := introduce("John", "laki-laki", "penulis", "30")
	fmt.Println(john) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

	sarah := introduce("Sarah", "perempuan", "model", "28")
	fmt.Println(sarah) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	// soal 3
	fmt.Println("=====SOAL 3=====")
	var buah = []string{"semangka", "jeruk", "melon", "pepaya"}

	var buahFavoritJohn = buahFavorit("john", buah...)

	fmt.Println(buahFavoritJohn)
	// halo nama saya john dan buah favorit saya adalah "semangka", "jeruk", "melon", "pepaya"

	// soal 4
	fmt.Println("=====SOAL 4=====")
	var dataFilm = []map[string]string{}
	// buatlah closure function disini

	tambahDataFilm := func(title, duration, genre, year string) {
		film := map[string]string{
			"title":    title,
			"duration": duration,
			"genre":    genre,
			"year":     year,
		}

		dataFilm = append(dataFilm, film)
	}

	tambahDataFilm("LOTR", "2 jam", "action", "1999")
	tambahDataFilm("avenger", "2 jam", "action", "2019")
	tambahDataFilm("spiderman", "2 jam", "action", "2004")
	tambahDataFilm("juon", "2 jam", "horror", "2004")

	for _, item := range dataFilm {
		fmt.Println(item)
	}

}

// soal 1
func luasPersegiPanjang(p, l int) string {

	return fmt.Sprintf("luas persegi panjang dari p=%v, l=%v adalah %v", p, l, p*l)
}

func kelilingPersegiPanjang(p, l int) int {
	return 2 * (p + l)
}

func volumeBalok(p, l, t int) int {
	return p * l * t
}

// soal 2
func introduce(name, gender, job, age string) (sentence string) {
	var prefix string

	if gender == "perempuan" {
		prefix = "Bu"
	} else {
		prefix = "Pak"
	}

	sentence = fmt.Sprintf("%v %v adalah seorang %v yang berusia %v tahun", prefix, name, job, age)
	return
}

// soal 3
func buahFavorit(nama string, buah ...string) string {
	var kalimatBuah string

	for index, item := range buah {
		if index == 0 {

			kalimatBuah = fmt.Sprintf("%q", item)
		} else {
			kalimatBuah = fmt.Sprintf("%v, %q ", kalimatBuah, item)
		}
	}

	return "halo nama saya " + nama + " dan buah favorit saya adalah " + kalimatBuah

}
