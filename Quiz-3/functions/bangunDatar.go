package functions

import (
	"math"
	"net/http"
	"quiz3/utils"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

func HitungBangunDatar(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	bangunDatar := ps.ByName("jenis-bangun-datar")
	hitung := r.URL.Query().Get("hitung")
	alas := r.URL.Query().Get("alas")
	tinggi := r.URL.Query().Get("tinggi")
	sisi := r.URL.Query().Get("sisi")
	panjang := r.URL.Query().Get("panjang")
	lebar := r.URL.Query().Get("lebar")
	jariJari := r.URL.Query().Get("jariJari")

	hitungBangunDatarChannel := make(chan map[string]interface{})

	if hitung != "luas" && hitung != "keliling" {
		http.Error(rw, "tolong masukkan luas atau keliling pada parameter hitung", http.StatusBadRequest)
		return
	}

	satuan := map[string]string{
		"hitung":   hitung,
		"alas":     alas,
		"tinggi":   tinggi,
		"sisi":     sisi,
		"panjang":  panjang,
		"lebar":    lebar,
		"jariJari": jariJari,
	}
	go hasilHitung(bangunDatar, satuan, hitungBangunDatarChannel)

	res := <-hitungBangunDatarChannel

	utils.ResponseJSON(rw, res, http.StatusAccepted)

}

func hasilHitung(bangunDatar string, satuan map[string]string, hitungCh chan map[string]interface{}) {
	jenisHitung := satuan["hitung"]

	result := map[string]interface{}{
		"bangun_datar": bangunDatar,
	}
	switch bangunDatar {
	case "segitiga-sama-sisi":
		{
			if jenisHitung == "luas" {
				alas, _ := strconv.Atoi(satuan["alas"])
				tinggi, _ := strconv.Atoi(satuan["tinggi"])
				result["alas"] = alas
				result["tinggi"] = tinggi
				result["luas"] = float64(alas * tinggi / 2)
				hitungCh <- result
			} else if jenisHitung == "keliling" {
				sisi, _ := strconv.Atoi(satuan["alas"])
				result["alas"] = sisi
				result["keliling"] = sisi * 3
				hitungCh <- result
			}
		}
	case "persegi":
		{
			sisi, _ := strconv.Atoi(satuan["sisi"])
			result["sisi"] = sisi
			if jenisHitung == "luas" {
				result["luas"] = sisi * sisi
				hitungCh <- result
			} else if jenisHitung == "keliling" {
				result["keliling"] = sisi * 4
				hitungCh <- result
			}
		}
	case "persegi-panjang":
		{
			panjang, _ := strconv.Atoi(satuan["panjang"])
			lebar, _ := strconv.Atoi(satuan["lebar"])
			result["panjang"] = panjang
			result["lebar"] = lebar
			if jenisHitung == "luas" {
				result["luas"] = panjang * lebar
				hitungCh <- result
			} else if jenisHitung == "keliling" {
				result["keliling"] = 2 * (panjang + lebar)
				hitungCh <- result
			}
		}
	case "lingkaran":
		{
			jariJari, _ := strconv.Atoi(satuan["jariJari"])
			if jenisHitung == "luas" {

				hitungCh <- map[string]interface{}{
					"jariJari": jariJari,
					"luas":     math.Round(math.Pi * float64(jariJari) * float64(jariJari)),
				}
			} else if jenisHitung == "keliling" {
				hitungCh <- map[string]interface{}{
					"jariJari": jariJari,
					"keliling": math.Round(2 * math.Pi * float64(jariJari)),
				}
			}
		}
	case "jajar-genjang":
		{
			alas, _ := strconv.Atoi(satuan["alas"])
			tinggi, _ := strconv.Atoi(satuan["tinggi"])
			sisi, _ := strconv.Atoi(satuan["sisi"])
			result["alas"] = alas

			if jenisHitung == "luas" {
				result["tinggi"] = tinggi
				result["luas"] = alas * tinggi
				hitungCh <- result
			} else if jenisHitung == "keliling" {
				result["sisi"] = sisi
				result["keliling"] = (2 * alas) + (2 * sisi)
				hitungCh <- result
			}
		}
	}

}
