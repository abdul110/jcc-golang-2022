package auth

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func BasicAuth(h httprouter.Handle) httprouter.Handle {
	return func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		// Get the Basic Authentication credentials
		user, password, hasAuth := r.BasicAuth()

		switch {
		case hasAuth && user == "admin" && password == "password":
			h(rw, r, ps)
		case hasAuth && user == "editor" && password == "secret":
			h(rw, r, ps)
		case hasAuth && user == "trainer" && password == "rahasia":
			h(rw, r, ps)
		default:
			{
				rw.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
				http.Error(rw, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			}
		}

	}
}
